using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float force;
    private Animator anim;
    private Rigidbody2D rb;



    void Start()
    {
        anim = GetComponent<Animator>();

        rb = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
        anim.SetFloat("speed", Input.GetAxis("Horizontal"));

        if (Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("jump");
            rb.AddForce(Vector2.up * force);

        }



        transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * 2, 0f, 0f);
    }
}
