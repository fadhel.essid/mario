using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public Text timeText;
    public float timeLeft;
    private int timeLeftInt;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime / .4f; // 1 game sec ~ 0.4 real time sec

    }
    public void SetHudTime()
    {
        timeLeftInt = Mathf.RoundToInt(timeLeft);
        timeText.text = timeLeftInt.ToString("D3");
    }
}
